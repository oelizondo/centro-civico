module ProjectsHelper
  def options_for repositories
    options_for_select repositories.map { |repo| [repo.name, repo.id] }
  end
end