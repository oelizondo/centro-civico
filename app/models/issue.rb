class Issue < ApplicationRecord
  belongs_to :repository
  belongs_to :user, optional: true
  has_many :comments
  scope :open, -> { where(state: 'open') }
end
