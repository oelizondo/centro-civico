class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :lockable, :timeoutable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable, :omniauthable

  has_many :repositories, dependent: :delete_all
  has_many :collaborations, dependent: :delete_all
  has_many :comments, dependent: :delete_all
  has_many :issues, dependent: :delete_all
  has_many :projects

  validates :username, uniqueness: true, presence: true

  private

  def self.from_omniauth(auth)
    user = User.find_by(email: auth.info.email)
    return merge_accounts(user, auth) if user && user.confirmed_at

    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.email = auth.info.email
      user.username = auth.info.nickname
      user.login = auth.info.nickname
      user.location = auth.extra.raw_info.location
      user.password = Devise.friendly_token[0,20]
      user.skip_confirmation!
      user.save!
    end
  end

  def self.merge_accounts(user, auth)
    user.provider = auth.provider
    user.uid = auth.uid
    user.username = auth.info.nickname
    user.login = auth.info.nickname
    user.location = auth.extra.raw_info.location
    user.save!
    return user
  end
end
