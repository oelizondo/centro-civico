class Reference < ApplicationRecord
  belongs_to :project, inverse_of: :references
end
