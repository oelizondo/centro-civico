class Project < ApplicationRecord
  enum category: [:op1, :op2, :op3]
  enum phase: [:planeación, :inicio, :final]
  belongs_to :repository
  belongs_to :user
  has_many :references, inverse_of: :project
  accepts_nested_attributes_for :references, reject_if: :all_blank, allow_destroy: true

  validates :name, :url, :category, :repository, :about, :phase, :introduction, :conclusion, presence: true
  validates :url, :format => URI::regexp(%w(http https))

  # scope :planning_phase, -> { where(phase: 'planeación') }

end
