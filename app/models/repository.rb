class Repository < ApplicationRecord
  belongs_to :user
  has_one :project
  has_many :issues
  has_many :branches
  has_many :collaborations
  has_many :users, through: :collaborations

  scope :popular, -> { order(stars: :desc) }
end
