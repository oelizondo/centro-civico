class CommentCreationJob < ApplicationJob
  queue_as :default

  def perform(user)
    comment_client = Github::Client::Issues::Comments.new
    issues = Issue.all

    issues.each do |issue|
      comments = comment_client.all user.login, issue.repository.name, number: issue.number
      comments.each do |comment|
        comment = filter_attributes(user, issue, comment)
        Comment.find_or_create_by(comment)
      end
    end
  end

  private

  def filter_attributes(user, issue, comment)
    {
      issue_id: issue.id,
      user: User.find_by(login: comment.user.login),
      body: comment.body
    }
  end
end
