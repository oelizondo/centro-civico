class IssueCreationJob < ApplicationJob
  queue_as :default

  def perform(user)
    issues_client = Github::Client::Issues::new
    repos = User.find(user.id).repositories

    repos.each do |repo|
      issues = issues_client.list user: user.login, repo: repo.name, state: 'open'
      issues.each do |issue|
        issue = filter_issue_attributes(user, repo, issue)
        Issue.find_or_create_by(issue)
      end
    end
  end

  private

  def filter_issue_attributes (user, repo, issue)
    {
      repository_id: repo.id,
      user: User.find_by(login: issue.user.login),
      owner: issue.user.login,
      number: issue.number,
      title: issue.title,
      state: issue.state,
      comment_count: issue.comments
    }
  end
end
