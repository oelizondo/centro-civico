class RepositoryCreationJob < ApplicationJob
  WEBHOOK_CONFIG = { "name" => "web", "active" => true, "config" => { "url" => Rails.application.secrets.payload_url } }
  queue_as :default

  def perform(user)
    repos = Github.repos.list(user: user.login, per_page: 100)
    repos.each_page do |page|
      page.each do |repo|
        filtered_repo = filter_attributes(user, repo)
        Repository.find_or_create_by(filtered_repo)
        # Github.repos.hooks.create(user.login, filtered_repo.name, WEBHOOK_CONFIG)
      end
    end
  end

  private

  def filter_attributes (user, repo)
    {
      user_id: user.id,
      name: repo.name.downcase,
      url: repo.html_url,
      description: repo.description,
      homepage: repo.homepage,
      language: repo.language,
      stars: repo.stargazers_count,
      default_branch: repo.default_branch,
      forks_count: repo.forks_count,
      size: repo.size
    }
  end
end