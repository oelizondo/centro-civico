class ApplicationJob < ActiveJob::Base
  def perform(user)
    RepositoryCreationJob.perform_later(user)
    IssueCreationJob.set(wait: 1.minute).perform_later(user)
    CommentCreationJob.set(wait: 3.minutes).perform_later(user)
  end
end
