class CallbacksController < Devise::OmniauthCallbacksController
  def github
    @user = User.from_omniauth(request.env["omniauth.auth"])
    ApplicationJob.perform_now(@user) if @user.repositories.count == 0
    sign_in_and_redirect @user
  end
end
