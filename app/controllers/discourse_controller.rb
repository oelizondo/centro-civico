require 'single_sign_on'

class DiscourseController < ApplicationController
  before_action :authenticate_user!
  def sso
    secret = "codeando_mexico_sso"
    sso = SingleSignOn.parse(request.query_string, secret)
    sso.email = current_user.email # from devise
    sso.name = current_user.login # this is a custom method on the User class
    sso.external_id = current_user.id # from devise
    sso.sso_secret = secret
    redirect_to sso.to_url("http://159.203.102.53:9000/session/sso_login")
  end
end
