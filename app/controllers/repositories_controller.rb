class RepositoriesController < ApplicationController
  before_action :authenticate_user!

  def index
    @repositories = current_user.repositories
  end

  def show
    @repository = current_user.repositories.find(params[:id])
    @issues = @repository.issues
  end

  def scoped_repositories
    @repositories = User.find_by(username: params[:username]).repositories
  end

  def show_scoped_repositories
    @repository = User.find_by(username: params[:username]).repositories.find_by(name: params[:repository])
  end
end
