class IssuesController < ApplicationController
  def index
    @issues = current_user.repositories.find(params[:repository_id]).issues.open
  end

  def show
    @issue = current_user.repositories.find(params[:repository_id]).issues.find(params[:id])
  end
end