class PagesController < ApplicationController
  before_action :authenticate_user!, only: [:profile, :repository]

  def index
  end

  def public_profile
    @user = User.find_by(username: params[:username])
  end

  def profile
    @name = get_showable_name
    @repos = current_user.repositories.popular.first(6)
  end

  def marketplace
    @projects = Project.all
  end

  private

  def get_showable_name
    current_user.login || current_user.username || current_user.email
  end
end
