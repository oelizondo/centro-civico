class OrganizationsController < ApplicationController
  def new
    @organization = Organization.new
  end

  def create
    @organization = Organization.create(organization_params)
    if @organization.save
      redirect_to organization_path
    else
      render 'new'
    end
  end

  def show
  end
  private

  def organization_params
    params.require(:organization).permit(:name, :site, :facebook, :twitter, :instagram, :github, :about, :category, :tag_list)
  end
end
