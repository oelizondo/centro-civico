class ProjectsController < ApplicationController
  def new
    @project = current_user.projects.build
    @user_repositories = current_user.repositories
  end

  def create
    @project = current_user.projects.build(project_params)
    if @project.save
      redirect_to project_path @project
    else
      redirect_to new_project_path
    end
  end

  def show
  end

  private

  def project_params
    params.require(:project).permit(:name, :url, :category, :repository_id, :about, :phase, :introduction, :conclusion, references_attributes: [:id, :url, :_destroy])
  end
end
