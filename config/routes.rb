Rails.application.routes.draw do
  devise_for :users, :controllers => { :omniauth_callbacks => 'callbacks' }
  root 'pages#index'
  get 'discourse/sso', to: 'discourse#sso'

  resources :repositories, only: [:index, :show] do
    resources :issues, only: [:index, :show] do
      resources :comments, only: :index
    end
  end

  resources :projects

  get '/marketplace', to: 'pages#marketplace', as: 'marketplace'
  get '/u/:username', to: 'pages#public_profile', as: 'public_profile'
  get '/u/:username/repositories', to: 'repositories#scoped_repositories', as: 'scoped_repositories'
  get '/u/:username/:repository', to: 'repositories#show_scoped_repositories', as: 'scoped_repository'
  get '/settings/profile', to: 'pages#profile', as: 'profile'
  post '/payload', to: 'github#payload', as: 'payload'
end
