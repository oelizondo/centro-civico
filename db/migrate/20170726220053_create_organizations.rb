class CreateOrganizations < ActiveRecord::Migration[5.0]
  def change
    create_table :organizations do |t|
      t.string :name
      t.string :site
      t.string :facebook
      t.string :twitter
      t.string :instagram
      t.string :github
      t.string :about
      t.integer :category

      t.timestamps
    end
  end
end
