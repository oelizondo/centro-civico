class CreateBranches < ActiveRecord::Migration[5.0]
  def change
    create_table :branches do |t|
      t.references :repository, foreign_key: true
      t.string :name
      t.string :sha

      t.timestamps
    end
  end
end
