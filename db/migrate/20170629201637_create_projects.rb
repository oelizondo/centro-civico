class CreateProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :projects do |t|
      t.string :name
      t.string :url
      t.integer :category
      t.string :about
      t.integer :phase
      t.date :introduction
      t.date :conclusion
      t.references :repository, foreign_key: true

      t.timestamps
    end
  end
end
