class AddOwnerToIssues < ActiveRecord::Migration[5.0]
  def change
    add_column :issues, :owner, :string
  end
end
