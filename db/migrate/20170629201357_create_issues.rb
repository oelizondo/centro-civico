class CreateIssues < ActiveRecord::Migration[5.0]
  def change
    create_table :issues do |t|
      t.references :repository, foreign_key: true
      t.references :user, foreign_key: true
      t.integer :number
      t.string :title
      t.string :state
      t.integer :comment_count

      t.timestamps
    end
  end
end
