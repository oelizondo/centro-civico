class CreateRepositories < ActiveRecord::Migration[5.0]
  def change
    create_table :repositories do |t|
      t.references :user, foreign_key: true
      t.string :name
      t.string :url
      t.string :description
      t.string :homepage
      t.string :language
      t.integer :stars
      t.string :default_branch
      t.integer :forks_count
      t.integer :size

      t.timestamps
    end
  end
end
