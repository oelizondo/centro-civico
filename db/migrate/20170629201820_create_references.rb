class CreateReferences < ActiveRecord::Migration[5.0]
  def change
    create_table :references do |t|
      t.references :project, foreign_key: true
      t.string :url

      t.timestamps
    end
  end
end
