require 'rails_helper'

describe 'Sign up Process', type: 'feature' do
  it 'signs up correctly' do
    visit 'users/sign_up'

    within('#new_user') do
      fill_in 'user_email', with: 'user@example.com'
      fill_in 'user_password', with: '12341234'
      fill_in 'user_password_confirmation', with: '12341234'

      click_button 'Sign up'
    end

    expect(page).to have_content 'tecnología'
  end

  it 'signs up incorrectly with passwords' do
    visit 'users/sign_up'

    within('#new_user') do
      fill_in 'user_email', with: 'user@example.com'
      fill_in 'user_password', with: '1234'
      fill_in 'user_password_confirmation', with: '1234'

      click_button 'Sign up'
    end

    expect(page).to have_content 'Sign up'
  end

  it 'signs up incorrectly with password confirmation' do
    visit 'users/sign_up'

    within('#new_user') do
      fill_in 'user_email', with: 'user@example.com'
      fill_in 'user_password', with: '12341234'
      fill_in 'user_password_confirmation', with: '43214321'

      click_button 'Sign up'
    end

    expect(page).to have_content 'Sign up'
  end


  it 'signs up incorrectly with email' do
    visit 'users/sign_up'

    within('#new_user') do
      fill_in 'user_email', with: 'user'
      fill_in 'user_password', with: '12341234'
      fill_in 'user_password_confirmation', with: '12341234'

      click_button 'Sign up'
    end

    expect(page).to have_content 'Sign up'
  end
end
