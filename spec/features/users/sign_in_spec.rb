require 'rails_helper'

describe 'the sign in process', type: 'feature' do
  before :each do
    User.create!(email: 'user@example.com', password: '12341234', confirmed_at: Time.now)
  end

  it 'signs in correctly' do
    visit 'users/sign_in'
    within('#new_user') do
      fill_in 'user_email', with: 'user@example.com'
      fill_in 'user_password', with: '12341234'

      click_button 'Log in'
    end

    expect(page).to have_content 'tecnología'
  end

  it 'signs in incorrectly' do
    visit 'users/sign_in'

    within('#new_user') do
      fill_in 'user_email', with: 'user@notanexample.com'
      fill_in 'user_password', with: '43124312'

      click_button 'Log in'
    end

    expect(page).to have_content 'Log in'
  end
end
