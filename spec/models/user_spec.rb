require 'rails_helper'

RSpec.describe User, type: 'model' do
  describe 'User creation' do
    it 'returns a created User' do
      user = User.create!(email: 'oscar@oscar.com',password: '12341234', confirmed_at: Time.now)
      expect(user.email).to eq('oscar@oscar.com')
    end
  end
end
